from thefuzz import process

def count_matching_search_terms(searches):
    matches = process.extract("Gogle", searches)
    more_than_90 = list(filter(lambda n: n[1] >= 90, matches))
    return len(more_than_90)


queries = [
    "Gogle",
    "Gogle",
    "Google",
    "search engine",
    "search engine Gogle",
    "search engine Google",
]

print(count_matching_search_terms(queries))
